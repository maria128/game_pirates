<?php


namespace BinaryStudioAcademy\Game\Harbors;

use BinaryStudioAcademy\Game\Characters\Ship;

interface HarborInterface
{
    public function fire(Ship $player, Ship $ship): string;
    public function aboard(Ship $player, Ship $ship);
    public function buy();
    public function whereami();
    public function harbor();
}
