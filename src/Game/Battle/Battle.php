<?php


namespace BinaryStudioAcademy\Game\Battle;

use BinaryStudioAcademy\Game\Characters\Ship;

class Battle
{
    /**
     * @var Ship
     */
    private $defender;
    /**
     * @var Ship
     */
    private $attacker;

    /**
     * @var bool
     */
    private $defenderWasLucky = false;
    /**
     * @var Ship
     */
    private $ship;
    /**
     * @var Ship
     */
    private $player;



    public function __construct(Ship $player, Ship $ship)
    {
        $this->ship = $ship;
        $this->player = $player;
    }

    public function startBattle()
    {
        $this->initAttacker($this->player);
        $this->initBeast($this->ship);

        $this->checkIfDefenderWasLucky();
        $this->updateDefenderHealth();

        $this->initAttacker($this->ship);
        $this->initBeast($this->player);

        $this->checkIfDefenderWasLucky();
        $this->updateDefenderHealth();

        return $this->printBattleResults();
    }

    public function initAttacker(Ship $attacker)
    {
        $this->attacker = $attacker;
    }

    public function initBeast(Ship $beast)
    {
        $this->defender = $beast;
    }

    public function updateDefenderHealth()
    {
        $damage = $this->attacker->getDamage();

        if (false === $this->defenderWasLucky) {
            $damage = 0;
        }
        $newHealthValue = $this->defender->getHealth() - $damage;

        if ($newHealthValue < 0) {
            $newHealthValue = 0;
        }
        $this->defender->setHealth($newHealthValue);
    }

    public function checkIfDefenderWasLucky()
    {
        $this->defenderWasLucky = $this->attacker->checkIfDefenderWasLucky();
    }


    public function printBattleResults()
    {
        if ($this->ship->getHealth() <= 0) {
            return $this->win();
        }

        if ($this->player->getHealth() <= 0) {
            return $this->die();
        }
        $shipName = $this->ship->getName();

        $playerDamage = $this->player->getDamage();
        $shipHealth = $this->ship->getHealth();
        $shipDamage = $this->ship->getDamage();
        $playerHealth = $this->player->getHealth();


        return "{$shipName} has damaged on: {$playerDamage} points." . \PHP_EOL
                . "health: {$shipHealth}" . \PHP_EOL
                . "{$shipName} damaged your ship on: {$shipDamage} points." . \PHP_EOL
                . "health: {$playerHealth}" . \PHP_EOL;
    }


    private function win(): string
    {
        $shipName = $this->ship->getName();

        return "{$shipName} on fire. Take it to the boarding." . \PHP_EOL;
    }

    private function die(): string
    {
        return 'Your ship has been sunk.' . \PHP_EOL
            . 'You restored in the Pirate Harbor.' . \PHP_EOL
            . 'You lost all your possessions and 1 of each stats.' . \PHP_EOL;
    }
}
