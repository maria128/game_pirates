<?php


namespace BinaryStudioAcademy\Game\Command;

class BuyCommand extends MainCommand implements CommandInterface
{
    public function configure()
    {
        $this->setName('buy <strength|armour|luck|rum>')
            ->setDescription('buys skill or rum: 1 chest of gold - 1 item')
            ->setParams(['strength', 'armour', 'luck', 'rum']);
    }

    public function execute()
    {
        if (!$this->checkParams()) {
            return $this->commandsGame->error();
        }

        return $this->commandsGame->buy($this->getCurrentParam());
    }
}
