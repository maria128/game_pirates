<?php


namespace BinaryStudioAcademy\Game\Command;

class ExitGameCommand extends MainCommand implements CommandInterface
{
    public function configure()
    {
        $this->setName('exit')->setDescription('finishes the game');
    }

    public function execute()
    {
        return $this->commandsGame->exitGame();
    }
}
