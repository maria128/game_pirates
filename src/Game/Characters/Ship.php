<?php


namespace BinaryStudioAcademy\Game\Characters;

use BinaryStudioAcademy\Game\Helpers\Math;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Helpers\Stats;

class Ship
{
    const RUM = 1;
    const GOLD = 2;
    private $name;
    private $strength;
    private $armour;
    private $health;
    private $luck;
    private $hold = [];
    private $math;
    private $random;
    private $typeShip;

    public function __construct(
        Random $random,
        string $name,
        int $strength,
        int $armour,
        int $health,
        int $luck,
        array $hold,
        $typeShip
    ) {
        $this->setName($name);
        $this->setArmour($armour);
        $this->setHealth($health);
        $this->setLuck($luck);
        $this->setStrength($strength);
        $this->setHold($hold);
        $this->math = new Math();
        $this->random = $random;
        $this->typeShip = $typeShip;
    }

    public function getTypeShip()
    {
        return $this->typeShip;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }


    public function getStrength()
    {
        return $this->strength;
    }
    public function setStrength($strength)
    {
        $this->strength =    $this->setCorrectValue(Stats::MAX_STRENGTH, Stats::MIN_STRENGTH, $strength);
        ;
    }

    public function getArmour()
    {
        return $this->armour;
    }

    public function setArmour($armour)
    {
        $this->armour = $this->setCorrectValue(Stats::MAX_ARMOUR, Stats::MIN_ARMOUR, $armour);
    }


    public function getLuck(): int
    {
        return $this->luck;
    }
    public function setLuck($luck)
    {
        $this->luck = $this->setCorrectValue(Stats::MAX_LUCK, Stats::MIN_LUCK, $luck);
    }

    public function getHealth()
    {
        return $this->health;
    }

    public function setHealth($health)
    {
        $this->health = $this->setCorrectValue(Stats::MAX_HEALTH, Stats::MIN_HEALTH, $health);
    }

    public function getHold()
    {
        return $this->hold;
    }

    public function getHoldString()
    {
        $string = '[';

        for ($i = 0; $i <= Stats::MAX_COUNT_HOLD; $i++) {
            $string.= $this->hold[$i] ?? ' _';
        }
        $string.= ' ]';

        return $string;
    }

    public function setHold($item)
    {
        $this->checkHold(Stats::MAX_COUNT_HOLD);
        $this->hold = $item;
    }

    public function getStat()
    {
        return 'Ship stats:' . \PHP_EOL
            . 'strength: ' . $this->getStrength() . \PHP_EOL
            . 'armour: ' . $this->getArmour() . \PHP_EOL
            . 'luck: ' . $this->getLuck() . \PHP_EOL
            . 'health: ' . $this->getHealth() . \PHP_EOL
            . 'hold: ' . $this->getHoldString() . \PHP_EOL;
    }


    public function getDamage()
    {
        return  $this->math->damage($this->getStrength(), $this->getArmour());
    }

    public function checkIfDefenderWasLucky()
    {
        if ($this->math->luck($this->random, $this->getLuck())) {
            $lucky = true;
        } else {
            $lucky = false;
        }

        return $lucky;
    }

    private function checkHold($maxCountHold)
    {
        if (\count($this->hold) > $maxCountHold) {
            return 'You have full hold';
        }

        return false;
    }


    private function setCorrectValue($maxValue, $minValue, $currentValue)
    {
        if (empty($maxValue) || empty($minValue) || empty($currentValue)) {
            $currentValue = 0;
        }

        if ($currentValue > $maxValue) {
            $currentValue = $maxValue;
        }

        if ($currentValue < $minValue) {
            $currentValue = $minValue;
        }

        return $currentValue;
    }
}
