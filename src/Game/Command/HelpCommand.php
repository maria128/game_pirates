<?php


namespace BinaryStudioAcademy\Game\Command;

class HelpCommand extends MainCommand implements CommandInterface
{
    public function configure()
    {
        $this->setName('help')->setDescription('shows this list of commands');
    }

    public function execute()
    {
        return  $this->commandsGame->help();
    }
}
