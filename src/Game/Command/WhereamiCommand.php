<?php


namespace BinaryStudioAcademy\Game\Command;

class WhereamiCommand extends MainCommand implements CommandInterface
{
    public function configure()
    {
        $this->setName('whereami')->setDescription('shows current harbor');
    }

    public function execute()
    {
        return $this->commandsGame->whereami();
    }
}
