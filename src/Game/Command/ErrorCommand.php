<?php


namespace BinaryStudioAcademy\Game\Command;

class ErrorCommand extends MainCommand implements CommandInterface
{
    public function configure()
    {
        $this->setName('error')->setDescription('Command \'unknown_command\' not found');
    }
    public function execute()
    {
        return $this->commandsGame->error();
    }
}
