<?php


namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Characters\Ship;
use BinaryStudioAcademy\Game\Command\AboardCommand;
use BinaryStudioAcademy\Game\Command\BuyCommand;
use BinaryStudioAcademy\Game\Command\CommandRegistry;
use BinaryStudioAcademy\Game\Command\CommandsGame;
use BinaryStudioAcademy\Game\Command\DrinkCommand;
use BinaryStudioAcademy\Game\Command\ErrorCommand;
use BinaryStudioAcademy\Game\Command\ExitGameCommand;
use BinaryStudioAcademy\Game\Command\FireCommand;
use BinaryStudioAcademy\Game\Command\HelpCommand;
use BinaryStudioAcademy\Game\Command\SetSailCommand;
use BinaryStudioAcademy\Game\Command\StatsCommand;
use BinaryStudioAcademy\Game\Command\WhereamiCommand;
use BinaryStudioAcademy\Game\Harbors\OtherHarbor;
use BinaryStudioAcademy\Game\Harbors\PiratesHarbor;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Helpers\Ships;

class ManagerGame
{
    const PIRATES_HARBOR = 1;

    /**
     * @var Ship
     */
    public static $piratesShip;
    public static $numberHarbor;

    /**
     * @var OtherHarbor|PiratesHarbor
     */
    public static $harbor;

    private $registry;
    private static $random;

    public function __construct(Random $random)
    {
        self::$random = $random;
        self::$numberHarbor = self::PIRATES_HARBOR;
        self::$piratesShip = self::getShip(self::PIRATES_HARBOR);
        self::$harbor = self::getHarbor(self::PIRATES_HARBOR);
    }

    public function registerCommand()
    {
        $commandsGame = new CommandsGame();
        $this->registry = new CommandRegistry();
        $this->registry->add(new HelpCommand($commandsGame), 'help');
        $this->registry->add(new StatsCommand($commandsGame), 'stats');
        $this->registry->add(new SetSailCommand($commandsGame), 'set-sail');
        $this->registry->add(new FireCommand($commandsGame), 'fire');
        $this->registry->add(new AboardCommand($commandsGame), 'aboard');
        $this->registry->add(new BuyCommand($commandsGame), 'buy');
        $this->registry->add(new DrinkCommand($commandsGame), 'drink');
        $this->registry->add(new WhereamiCommand($commandsGame), 'whereami');
        $this->registry->add(new ExitGameCommand($commandsGame), 'exit');
        $this->registry->add(new ErrorCommand($commandsGame), 'error');

        return $this->registry;
    }


    public static function getHarbor($number)
    {
        switch ($number) {
            case 1:
                $harbor = new PiratesHarbor($number, 'Pirates Harbor', self::getShip($number), ['north' => 4, 'west' => 3, 'south' => 2]);
                break;
            case 2:
                $harbor = new OtherHarbor($number, 'Southhampton', self::getShip($number), ['north' => 1, 'west' => 3, 'east' => 7]);
                break;
            case 3:
                $harbor = new OtherHarbor($number, 'Fishguard', self::getShip($number), ['east' => 1, 'south' => 2, 'north' => 4]);
                break;
            case 4:
                $harbor = new OtherHarbor($number, 'Salt End', self::getShip($number), ['west' => 3, 'south' => 1, 'east' => 5]);
                break;
            case 5:
                $harbor = new OtherHarbor($number, 'Isle of Grain', self::getShip($number), ['west' => 4, 'south' => 7, 'east' => 6]);
                break;
            case 6:
                $harbor = new OtherHarbor($number, 'Grays', self::getShip(6), ['west' => 5, 'south' => 8]);
                break;
            case 7:
                $harbor = new OtherHarbor($number, 'Felixstowe', self::getShip($number), ['west' => 2, 'north' => 5, 'east' => 8]);
                break;
            case 8:
                $harbor = new OtherHarbor($number, 'London Docks', self::getShip($number), ['west' => 7, 'north' => 6]);
                break;
        }

        return $harbor;
    }

    public static function getShip($number)
    {
        switch ($number) {
            case 1:
                $ship = new Ship(
                    self::$random,
                    Ships::SHIPS['pirates']['name'],
                    Ships::SHIPS['pirates']['stats']['strength'],
                    Ships::SHIPS['pirates']['stats']['armour'],
                    Ships::SHIPS['pirates']['stats']['health'],
                    Ships::SHIPS['pirates']['stats']['luck'],
                    [],
                    'player'
                );
                break;
            case 2:
            case 3:
            case 4:
            case 5:
                $ship = new Ship(
                    self::$random,
                    Ships::SHIPS['schooner']['name'],
                    Ships::SHIPS['schooner']['stats']['strength'],
                    Ships::SHIPS['schooner']['stats']['armour'],
                    Ships::SHIPS['schooner']['stats']['health'],
                    Ships::SHIPS['schooner']['stats']['luck'],
                    [Ship::GOLD],
                    'schooner'
                );
                break;
            case 6:
            case 7:
                $ship = new Ship(
                    self::$random,
                    Ships::SHIPS['battle']['name'],
                    Ships::SHIPS['battle']['stats']['strength'],
                    Ships::SHIPS['battle']['stats']['armour'],
                    Ships::SHIPS['battle']['stats']['health'],
                    Ships::SHIPS['battle']['stats']['luck'],
                    [Ship::RUM],
                    'battle'
                );
                break;
            case 8:
                $ship = new Ship(
                    self::$random,
                    Ships::SHIPS['royal']['name'],
                    Ships::SHIPS['royal']['stats']['strength'],
                    Ships::SHIPS['royal']['stats']['armour'],
                    Ships::SHIPS['royal']['stats']['health'],
                    Ships::SHIPS['royal']['stats']['luck'],
                    [Ship::GOLD, Ship::GOLD, Ship::RUM],
                    'royal'
                );
                break;
        }

        return $ship;
    }

    public static function restartGame()
    {
        self::$numberHarbor = self::PIRATES_HARBOR;
        $oldValuePiratesShip = self::$piratesShip;
        self::$piratesShip = new Ship(
            self::$random,
            Ships::SHIPS['pirates']['name'],
            $oldValuePiratesShip->getStrength() - 1,
            $oldValuePiratesShip->getArmour() - 1,
            Ships::SHIPS['pirates']['stats']['health'],
            $oldValuePiratesShip->getLuck() - 1,
            [],
            'player'
        );
        ;
        self::$harbor = new PiratesHarbor(self::PIRATES_HARBOR, 'Pirates Harbor', self::$piratesShip, ['north' => 4, 'west' => 3, 'south' => 2]);
    }
}
