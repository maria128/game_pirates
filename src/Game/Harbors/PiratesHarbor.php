<?php


namespace BinaryStudioAcademy\Game\Harbors;

use BinaryStudioAcademy\Game\Characters\Ship;
use BinaryStudioAcademy\Game\Helpers\ErrorsGame;

class PiratesHarbor extends AbstractHarbor implements HarborInterface
{
    public function fire(Ship $player, Ship $ship): string
    {
        return ErrorsGame::errors('pirate_harbor_fire');
    }

    public function aboard(Ship $player, Ship $ship)
    {
        return ErrorsGame::errors('pirate_harbor_aboard');
    }

    public function buy()
    {
        // TODO: Implement buy() method.
    }
}
