<?php

namespace BinaryStudioAcademy\Game\Contracts\Io;

interface Reader
{
    public function read(): string;

    /**
     * @throws \Exception
     *
     * @return resource
     */
    public function getStream();
}
