<?php


namespace BinaryStudioAcademy\Game\Helpers;

class Ships
{
    const SHIPS = [
        'pirates' => [
            'name' => 'Pirates Harbor',
            'stats' => [
                'strength' => 4,
                'armour' => 4,
                'luck' => 4,
                'health' => 60,
            ],
        ],
        'schooner' => [
            'name' => 'Royal Patrool Schooner',
            'stats' => [
                'strength' => 4,
                'armour' => 4,
                'luck' => 4,
                'health' => 50,
            ],
        ],
        'battle' => [
            'name' => 'Royal Battle Ship',
            'stats' => [
                'strength' => 8,
                'armour' => 8,
                'luck' => 7,
                'health' => 80,
            ],
        ],
        'royal' => [
            'name' => 'HMS Royal Sovereign',
            'stats' => [
                'strength' => 10,
                'armour' => 10,
                'luck' => 10,
                'health' => 100,
            ],
        ],
    ];
}
