<?php


namespace BinaryStudioAcademy\Game\Harbors;

use BinaryStudioAcademy\Game\Battle\Battle;
use BinaryStudioAcademy\Game\Characters\Ship;
use BinaryStudioAcademy\Game\Helpers\ErrorsGame;

class OtherHarbor extends AbstractHarbor implements HarborInterface
{
    public function fire(Ship $player, Ship $ship): string
    {
        $battle =  new Battle($player, $ship);

        return $battle->startBattle();
    }

    public function aboard(Ship $player, Ship $ship)
    {
        for ($i = 0; $i < \count($ship->getHold()); $i++) {
            if (\count($player->getHold()) >= 3) {
                break;
            }
            \array_push($player->getHold(), $ship->getHold()[$i]);
        }
    }

    public function buy()
    {
        ErrorsGame::errors('buy_in_not_pirate_harbor');
    }
}
