<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;

class Game
{
    /**
     * @var Random
     */
    private $random;
    private $registry;

    public function __construct(Random $random)
    {
        $this->random = $random;
        $init = new ManagerGame($random);

        $this->registry = $init->registerCommand();
    }

    public function start(Reader $reader, Writer $writer)
    {
        $writer->writeln('Your task is to develop a game "Battle Ship".');
        $writer->writeln('This method starts infinite loop with game logic.');
        $writer->writeln('Use proposed implementation in order to tests work correct.');
        $writer->writeln('Random float number: ' . $this->random->get());

        $input = \trim($reader->read());

        $writer->writeln($this->registry->get($input)->execute());
        $writer->writeln('Adventure has begun. Wish you good luck!');
    }

    public function run(Reader $reader, Writer $writer)
    {
        $input = \trim($reader->read());
        $string = $this->registry->get($input)->execute();
        $writer->writeln($string);
    }
}
