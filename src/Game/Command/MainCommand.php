<?php


namespace BinaryStudioAcademy\Game\Command;

class MainCommand
{
    private $name;
    private $description;
    private $params;
    private $currentParam;

    /**
     * @var CommandsGame
     */
    protected $commandsGame;
    protected $option;

    public function __construct(CommandsGame $commandsGame)
    {
        $this->commandsGame = $commandsGame;
        $this->configure();
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function setCurrentParam($params)
    {
        $this->currentParam = $params;
    }

    public function getCurrentParam()
    {
        return $this->currentParam;
    }

    public function getName()
    {
        return $this->name;
    }

    protected function setName($name)
    {
        $this->name = $name;

        return $this;
    }
    protected function setParams(array $params)
    {
        $this->params = $params;

        return $this;
    }
    protected function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    protected function configure()
    {
    }

    protected function checkParams()
    {
        if (\in_array($this->getCurrentParam(), $this->getParams())) {
            return true;
        }

        return false;
    }
}
