<?php


namespace BinaryStudioAcademy\Game\Command;

class DrinkCommand extends MainCommand implements CommandInterface
{
    public function configure()
    {
        $this->setName('drink')->setDescription('your captain drinks 1 bottle of rum and fill 30 points of health');
    }

    public function execute()
    {
        return $this->commandsGame->drink();
    }
}
