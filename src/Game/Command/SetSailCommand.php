<?php


namespace BinaryStudioAcademy\Game\Command;

class SetSailCommand extends MainCommand implements CommandInterface
{
    public function configure()
    {
        $this->setName('set-sail <east|west|north|south>')
            ->setDescription('moves in given direction')
        ->setParams(['east', 'west', 'north', 'south']);
    }


    public function execute()
    {
        if (!$this->checkParams()) {
            return $this->commandsGame->error();
        }

        return $this->commandsGame->setSail($this->getCurrentParam());
    }
}
