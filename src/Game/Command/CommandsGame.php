<?php


namespace BinaryStudioAcademy\Game\Command;

use BinaryStudioAcademy\Game\Characters\Ship;
use BinaryStudioAcademy\Game\Helpers\Stats;
use BinaryStudioAcademy\Game\ManagerGame;
use BinaryStudioAcademy\Game\Helpers\ErrorsGame;

class CommandsGame
{
    public function help()
    {
        $commands = new CommandRegistry();
        $string = 'List of commands:' . \PHP_EOL;

        foreach ($commands->getAllRegistryCommands() as $command) {
            $string .= $command->getName() . ' - ' . $command->getDescription() . \PHP_EOL;
        }

        return $string;
    }
    public function stats()
    {
        return ManagerGame::$piratesShip->getStat();
    }
    public function setSail($direction)
    {
        if (!(ManagerGame::$harbor->checkDirection($direction))) {
            return ErrorsGame::errors('incorrect_direction');
        }

        ManagerGame::$numberHarbor = ManagerGame::$harbor->getDirection()[$direction];

        ManagerGame::$harbor = ManagerGame::getHarbor(ManagerGame::$numberHarbor);

        if (ManagerGame::PIRATES_HARBOR == ManagerGame::$numberHarbor) {
            return 'Harbor 1: Pirates Harbor.';
        }

        return ManagerGame::$harbor->harbor();
    }
    public function fire()
    {
        if (ManagerGame::$harbor->getShip()->getHealth() <= 0) {
            return  ErrorsGame::errors('pirate_harbor_fire');
        }

        $fire = ManagerGame::$harbor->fire(ManagerGame::$piratesShip, ManagerGame::$harbor->getShip());

        if (ManagerGame::$piratesShip->getHealth() <= 0) {
            ManagerGame::restartGame();
        }

        if (
            ManagerGame::$harbor->getShip()->getHealth() <= 0 &&
            'royal' == ManagerGame::$harbor->getShip()->getTypeShip()
        ) {
            return '🎉🎉🎉Congratulations🎉🎉🎉' . \PHP_EOL
                . '💰💰💰 All gold and rum of Great Britain belong to you! 🍾🍾🍾';
        }

        return $fire;
    }
    public function aboard()
    {
        if (ManagerGame::$harbor->getShip()->getHealth() > 0) {
            return  ErrorsGame::errors('aboard_live_ship');
        }

        if (3 == \count(ManagerGame::$piratesShip->getHold())) {
            return  ErrorsGame::errors('abord_full');
        }

        ManagerGame::$harbor->aboard(ManagerGame::$piratesShip, ManagerGame::$harbor->getShip());

        return 'You got 💰.' . \PHP_EOL;
    }
    public function buy($params)
    {
        return 'test';
    }
    public function drink()
    {
        $hold = ManagerGame::$piratesShip->getHold();

        if (Stats::MAX_HEALTH == ManagerGame::$piratesShip->getHealth()) {
            return  ErrorsGame::errors('full_health');
        }

        if (!$position = \array_search(Ship::RUM, $hold)) {
            return  ErrorsGame::errors('have_not_rum');
        }
        ManagerGame::$piratesShip->setHealth(ManagerGame::$piratesShip->getHealth() + Stats::ADD_HEALTH_DRINK_RUM);
        unset($hold[$position]);
        $health = ManagerGame::$piratesShip->getHealth();

        return "You\'ve drunk a rum and your health filled to {$health}";
    }
    public function whereami()
    {
        return ManagerGame::$harbor->whereAmI();
    }
    public function exitGame()
    {
        return 'test';
    }
    public function error()
    {
        return ErrorsGame::errors('unknown_command');
    }
}
