<?php

namespace BinaryStudioAcademy\Game\Helpers;

final class Stats
{
    public const MAX_LUCK = 10;
    public const MIN_LUCK = 1;
    public const MAX_STRENGTH = 10;
    public const MIN_STRENGTH = 1;
    public const MAX_ARMOUR = 10;
    public const MIN_ARMOUR = 1;
    public const MAX_HEALTH = 100;
    public const MIN_HEALTH = 1;
    public const MAX_COUNT_HOLD = 3;
    public const ADD_HEALTH_DRINK_RUM = 30;
}
