<?php


namespace BinaryStudioAcademy\Game\Harbors;

use BinaryStudioAcademy\Game\Characters\Ship;

abstract class AbstractHarbor
{
    protected $number;
    protected $name;
    protected $ship;
    protected $direction;


    public function __construct($number, $name, Ship $ship, $direction)
    {
        $this->name = $name;
        $this->number = $number;
        $this->ship = $ship;
        $this->direction = $direction;
    }

    public function whereami(): string
    {
        return "Harbor {$this->number}: {$this->name}" . \PHP_EOL;
    }

    public function harbor(): string
    {
        return "Harbor {$this->number}: {$this->name}." . \PHP_EOL
            . "You see {$this->ship->getName()}: " . \PHP_EOL
            . 'strength: ' . $this->ship->getStrength() . \PHP_EOL
            . 'armour: ' . $this->ship->getArmour() . \PHP_EOL
            . 'luck: ' . $this->ship->getLuck() . \PHP_EOL
            . 'health: ' . $this->ship->getHealth() . \PHP_EOL;
    }

    public function getDirection()
    {
        return $this->direction;
    }

    public function getShip()
    {
        return $this->ship;
    }

    public function checkDirection($direction)
    {
        return \array_key_exists($direction, $this->direction);
    }
}
