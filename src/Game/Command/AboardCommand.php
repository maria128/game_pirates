<?php


namespace BinaryStudioAcademy\Game\Command;

class AboardCommand extends MainCommand implements CommandInterface
{
    protected function configure()
    {
        $this->setName('aboard')->setDescription('collect loot from the ship');
    }

    public function execute()
    {
        return $this->commandsGame->aboard();
    }
}
