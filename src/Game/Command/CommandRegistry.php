<?php

namespace BinaryStudioAcademy\Game\Command;

class CommandRegistry
{
    private static $registry = [];

    public function add(CommandInterface $command, $type)
    {
        self::$registry[$type] = $command;
    }

    public function get($type)
    {
        $arguments = \explode(' ', $type);

        if (!isset(self::$registry[$arguments[0]])) {
            return self::$registry['error'];
        }

        $command = self::$registry[$arguments[0]];

        if (\count($arguments) > 1 && $arguments[1]) {
            $command->setCurrentParam($arguments[1]);
        }

        return self::$registry[$arguments[0]];
    }

    public function getAllRegistryCommands()
    {
        return self::$registry;
    }
}
