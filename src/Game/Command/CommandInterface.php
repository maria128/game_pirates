<?php

namespace BinaryStudioAcademy\Game\Command;

interface CommandInterface
{
    public function execute();
    public function getParams();
    public function setCurrentParam($param);
}
