<?php


namespace BinaryStudioAcademy\Game\Command;

class FireCommand extends MainCommand implements CommandInterface
{
    public function configure()
    {
        $this->setName('fire')->setDescription('attacks enemy\'s ship');
    }

    public function execute()
    {
        return $this->commandsGame->fire();
    }
}
