<?php


namespace BinaryStudioAcademy\Game\Command;

class StatsCommand extends MainCommand implements CommandInterface
{
    public function configure()
    {
        $this->setName('stats')->setDescription('shows stats of ship');
    }


    public function execute()
    {
        return $this->commandsGame->stats();
    }
}
