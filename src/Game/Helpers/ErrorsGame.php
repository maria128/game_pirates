<?php


namespace BinaryStudioAcademy\Game\Helpers;

class ErrorsGame
{
    public static function errors(string $key): string
    {
        return [
            'incorrect_direction' => 'Harbor not found in this direction',
            'pirate_harbor_aboard' => 'There is no ship to aboard',
            'pirate_harbor_fire' => 'There is no ship to fight',
            'buy_in_not_pirate_harbor' => 'You can buy only in pirate harbor',
            'aboard_live_ship' => 'You cannot board this ship, since it has not yet sunk',
            'incorrect_direction_command' => "Direction 'asd' incorrect, choose from: east, west, north, south",
            'abord_full hold' => 'You have full hold',
            'full_health' => 'You have full health',
            'have_not_rum' => 'You don\'t have rum',
            'unknown_command' => "Command 'unknown_command' not found",
        ][$key];
    }
}
